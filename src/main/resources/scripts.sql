CREATE DATABASE OAUTH_JWT;
USE OAUTH_JWT;

CREATE TABLE oauth_client_details (
    client_id varchar(255) NOT NULL,
    resource_ids varchar(255),
    client_secret varchar(255),
    scope varchar(255),
    authorized_grant_types varchar(255),
    web_server_redirect_uri varchar(255),
    authorities varchar(255),
    access_token_validity integer,
    refresh_token_validity integer,
    additional_information varchar(4096),
    autoapprove varchar(255)
);

INSERT INTO OAUTH_CLIENT_DETAILS(CLIENT_ID, RESOURCE_IDS, CLIENT_SECRET, SCOPE, AUTHORIZED_GRANT_TYPES, AUTHORITIES, ACCESS_TOKEN_VALIDITY, REFRESH_TOKEN_VALIDITY)
 VALUES ('spring-security-oauth2-read-client', 'resource-server-rest-api',
 /*spring-security-oauth2-read-client-password1234*/'$2a$04$WGq2P9egiOYoOFemBRfsiO9qTcyJtNRnPKNBl5tokP7IP.eZn93km',
 'read', 'password,authorization_code,refresh_token,implicit', 'USER', 10800, 2592000);
INSERT INTO OAUTH_CLIENT_DETAILS(CLIENT_ID, RESOURCE_IDS, CLIENT_SECRET, SCOPE, AUTHORIZED_GRANT_TYPES, AUTHORITIES, ACCESS_TOKEN_VALIDITY, REFRESH_TOKEN_VALIDITY)
 VALUES ('spring-security-oauth2-read-write-client', 'resource-server-rest-api',
 /*spring-security-oauth2-read-write-client-password1234*/'$2a$04$soeOR.QFmClXeFIrhJVLWOQxfHjsJLSpWrU1iGxcMGdu.a5hvfY4W',
 'read,write', 'password,authorization_code,refresh_token,implicit', 'USER', 10800, 2592000);


 
CREATE TABLE authority (
    id bigint NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name varchar(255)
);

INSERT INTO AUTHORITY(ID, NAME) VALUES (1, 'ADMIN');
INSERT INTO AUTHORITY(ID, NAME) VALUES (2, 'USER');


CREATE TABLE user_ (
    id bigint NOT NULL AUTO_INCREMENT PRIMARY KEY,
    account_expired boolean,
    account_locked boolean,
    credentials_expired boolean,
    enabled boolean,
    password varchar(255),
    user_name varchar(255)
);

INSERT INTO USER_(ID, USER_NAME, PASSWORD, ACCOUNT_EXPIRED, ACCOUNT_LOCKED, CREDENTIALS_EXPIRED, ENABLED)
  VALUES (1, 'admin', /*admin1234*/'$2a$08$qvrzQZ7jJ7oy2p/msL4M0.l83Cd0jNsX6AJUitbgRXGzge4j035ha', FALSE, FALSE, FALSE, TRUE);
INSERT INTO USER_(ID, USER_NAME, PASSWORD, ACCOUNT_EXPIRED, ACCOUNT_LOCKED, CREDENTIALS_EXPIRED, ENABLED)
  VALUES (2, 'reader', /*reader1234*/'$2a$08$dwYz8O.qtUXboGosJFsS4u19LHKW7aCQ0LXXuNlRfjjGKwj5NfKSe', FALSE, FALSE, FALSE, TRUE);
INSERT INTO USER_(ID, USER_NAME, PASSWORD, ACCOUNT_EXPIRED, ACCOUNT_LOCKED, CREDENTIALS_EXPIRED, ENABLED)
  VALUES (3, 'modifier', /*modifier1234*/'$2a$08$kPjzxewXRGNRiIuL4FtQH.mhMn7ZAFBYKB3ROz.J24IX8vDAcThsG', FALSE, FALSE, FALSE, TRUE);
 
  
CREATE TABLE users_authorities (
    user_id bigint NOT NULL,
    authority_id bigint NOT NULL
);


INSERT INTO USERS_AUTHORITIES(USER_ID, AUTHORITY_ID) VALUES (1, 1);
INSERT INTO USERS_AUTHORITIES(USER_ID, AUTHORITY_ID) VALUES (1, 2);
INSERT INTO USERS_AUTHORITIES(USER_ID, AUTHORITY_ID) VALUES (1, 3);
INSERT INTO USERS_AUTHORITIES(USER_ID, AUTHORITY_ID) VALUES (1, 4);
INSERT INTO USERS_AUTHORITIES(USER_ID, AUTHORITY_ID) VALUES (1, 5);
INSERT INTO USERS_AUTHORITIES(USER_ID, AUTHORITY_ID) VALUES (1, 6);
INSERT INTO USERS_AUTHORITIES(USER_ID, AUTHORITY_ID) VALUES (1, 7);
INSERT INTO USERS_AUTHORITIES(USER_ID, AUTHORITY_ID) VALUES (1, 8);
INSERT INTO USERS_AUTHORITIES(USER_ID, AUTHORITY_ID) VALUES (2, 6);
INSERT INTO USERS_AUTHORITIES(USER_ID, AUTHORITY_ID) VALUES (3, 5);
INSERT INTO USERS_AUTHORITIES(USER_ID, AUTHORITY_ID) VALUES (3, 6);
INSERT INTO USERS_AUTHORITIES(USER_ID, AUTHORITY_ID) VALUES (3, 7);

CREATE TABLE oauth_access_token (
    token_id varchar(255),
    token varbinary(25000),
    authentication_id varchar(255) NOT NULL,
    user_name varchar(255),
    client_id varchar(255),
    authentication varbinary(25000),
    refresh_token varchar(255)
);

CREATE TABLE oauth_approvals (
    userid varchar(255),
    clientid varchar(255),
    scope varchar(255),
    status varchar(10),
    expiresat datetime,
    lastmodifiedat datetime
);

CREATE TABLE oauth_client_token (
    token_id varchar(255),
    token varbinary(25000),
    authentication_id varchar(255) NOT NULL,
    user_name varchar(255),
    client_id varchar(255)
);

CREATE TABLE oauth_code (
    code varchar(255),
    authentication varbinary(25000)
);

CREATE TABLE oauth_refresh_token (
    token_id varchar(255),
    token varbinary(25000),
    authentication varbinary(25000)
);

ALTER TABLE authority
    ADD CONSTRAINT authority_name UNIQUE (name);

ALTER TABLE oauth_access_token
    ADD CONSTRAINT oauth_access_token_pkey PRIMARY KEY (authentication_id);
 

ALTER TABLE oauth_client_details
    ADD CONSTRAINT oauth_client_details_pkey PRIMARY KEY (client_id);


ALTER TABLE oauth_client_token
    ADD CONSTRAINT oauth_client_token_pkey PRIMARY KEY (authentication_id);