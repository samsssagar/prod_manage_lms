package com.hexaware.learningmanagementsystem.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hexaware.learningmanagementsystem.model.Course;

@Repository
public interface CourseRepository extends JpaRepository<Course, Long>{
	
	public Course findByCourseId(Long courseId);
	
	public Course findByCourseName(String courseName);
	
}
