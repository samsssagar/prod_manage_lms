package com.hexaware.learningmanagementsystem.service;

import java.util.List;

import com.hexaware.learningmanagementsystem.model.security.User;

public interface UserService {
	
	User save(User user);
	
	List<User> findAll();
	
	void delete(Long id);
	
	User getById(Long id);
	
}
