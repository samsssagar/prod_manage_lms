package com.hexaware.learningmanagementsystem.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hexaware.learningmanagementsystem.model.Course;
import com.hexaware.learningmanagementsystem.repository.CourseRepository;

@Service
public class CourseServiceImpl implements CourseService{
	
	@Autowired
	private CourseRepository courseRepository;
	
	@Override
	@Transactional(readOnly = true)
	@PreAuthorize("hasAuthority('COURSE_READ') and hasAuthority('COURSE_CREATE')")
	public Course get(Long id) {
		return this.courseRepository.findByCourseId(id);
	}

	@Override
	@Transactional(readOnly = true)
	@PreAuthorize("hasAuthority('COURSE_READ') and hasAuthority('COURSE_CREATE')")
	public Course get(String name) {
		return this.courseRepository.findByCourseName(name);
	}

	@Override
	@Transactional(readOnly = true)
	@PreAuthorize("hasAuthority('COURSE_READ') and hasAuthority('COURSE_CREATE')")
	public List<Course> getAll() {
		return this.courseRepository.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	@PreAuthorize("hasAuthority('COURSE_CREATE')")
	public void create(Course course) {
		this.courseRepository.save(course);
	}

	@Override
	@Transactional(readOnly = true)
	@PreAuthorize("hasAuthority('COURSE_UPDATE')")
	public Course update(Course course) {
		return null;
	}

	@Override
	@Transactional(readOnly = true)
	@PreAuthorize("hasAuthority('COURSE_DELETE')")
	public void delete(Long id) {
		this.courseRepository.deleteById(id);
	}

	@Override
	@Transactional(readOnly = true)
	@PreAuthorize("hasAuthority('COURSE_DELETE')")
	public void delete(Course course) {
		this.courseRepository.delete(course);
	}

}
