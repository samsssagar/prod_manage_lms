package com.hexaware.learningmanagementsystem.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hexaware.learningmanagementsystem.model.security.User;
import com.hexaware.learningmanagementsystem.repository.UserRepository;

@Service
public class UserDetailsServiceImpl implements UserDetailsService, UserService{

	@Autowired
	private UserRepository userRepository;
	
	
	@Override
	@Transactional(readOnly = true)
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = userRepository.findByUsername(username);
		if(user != null) {
			return user;
		}
		throw new UsernameNotFoundException(username);
	}


	@Override
	@Transactional(readOnly = true)
	@PreAuthorize("hasAuthority('ADMIN')")
	public User save(User user) {
		return this.userRepository.save(user);
	}


	@Override
	@Transactional(readOnly = true)
	@PreAuthorize("hasAuthority('ADMIN')")
	public List<User> findAll() {
		return this.userRepository.findAll();
	}


	@Override
	@Transactional(readOnly = true)
	@PreAuthorize("hasAuthority('ADMIN')")
	public void delete(Long id) {
		this.userRepository.deleteById(id);
	}


	@Override
	@Transactional(readOnly = true)
	@PreAuthorize("hasAuthority('ADMIN')")
	public User getById(Long id) {
		Optional<User> optional = userRepository.findById(id);
		User user = optional.get();
		return user;
	}
	
}
