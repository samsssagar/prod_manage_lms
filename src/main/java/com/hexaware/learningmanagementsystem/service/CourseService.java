package com.hexaware.learningmanagementsystem.service;

import java.util.List;

import com.hexaware.learningmanagementsystem.model.Course;

public interface CourseService {
	
	Course get(Long id);
	
	Course get(String name);
	
	List<Course> getAll();
	
	void create(Course course);
	
	Course update(Course course);
	
	void delete(Long id);
	
	void delete(Course course);
}
