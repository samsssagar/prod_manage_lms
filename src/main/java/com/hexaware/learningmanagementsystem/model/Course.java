package com.hexaware.learningmanagementsystem.model;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "Course")
public class Course {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long courseId;
	
	private String courseName;
	
	private String courseDescription;
	
	private String courseTitle;
	
	@ManyToMany
	@JoinTable(name = "student_course", joinColumns = @JoinColumn(name = "course_id", referencedColumnName = "courseId"), inverseJoinColumns = @JoinColumn(name = "student_id", referencedColumnName = "studentId"))
	private Set<Student> student;
	
	
	@ManyToMany
	@JoinTable(name = "teacher_course", joinColumns = @JoinColumn(name = "course_id",referencedColumnName = "courseId"), inverseJoinColumns = @JoinColumn(name = "faculty_id", referencedColumnName = "facultyId"))
	private Set<Faculty> faculty;


	public Long getCourseId() {
		return courseId;
	}


	public void setCourseId(Long courseId) {
		this.courseId = courseId;
	}


	public String getCourseName() {
		return courseName;
	}


	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}


	public String getCourseDescription() {
		return courseDescription;
	}


	public void setCourseDescription(String courseDescription) {
		this.courseDescription = courseDescription;
	}


	public String getCourseTitle() {
		return courseTitle;
	}


	public void setCourseTitle(String courseTitle) {
		this.courseTitle = courseTitle;
	}


	public Set<Student> getStudent() {
		return student;
	}


	public void setStudent(Set<Student> student) {
		this.student = student;
	}


	public Set<Faculty> getFaculty() {
		return faculty;
	}


	public void setFaculty(Set<Faculty> faculty) {
		this.faculty = faculty;
	}
	
	
	// for serialization
	public Course() {
		
	}
	
}
