package com.hexaware.learningmanagementsystem.controller;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.hexaware.learningmanagementsystem.model.Course;
import com.hexaware.learningmanagementsystem.service.CourseService;

@RestController
@RequestMapping("/secured/courses")
public class CourseController {
	
	@Autowired
	private CourseService courseService;
	
	@GetMapping
	@ResponseStatus(value = HttpStatus.OK)
	public List<Course> getAll() {
		return this.courseService.getAll();
	}
	
	@GetMapping("/{id}")
	@ResponseStatus(value = HttpStatus.OK)
	public Course get(@PathVariable Long id) {
		return this.courseService.get(id);
	}
	
	@GetMapping("/{name}")
	@ResponseStatus(value = HttpStatus.OK)
	public Course get(@PathVariable String name) {
		return this.courseService.get(name);
	}
	
	@PostMapping
	@ResponseStatus(value = HttpStatus.OK)
	public void create(@RequestBody Course course) {
		this.courseService.create(course);
	}
	
	@PutMapping("/{courseId}")
	public Course update(Course course, Long courseId) {
		return this.courseService.update(course);
	}
	
	
	@DeleteMapping("/{id}")
	@Transactional
	public void delete(@PathVariable Long id) {
		this.courseService.delete(id);
	}
	
	
}
