package com.hexaware.learningmanagementsystem.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.hexaware.learningmanagementsystem.model.security.User;
import com.hexaware.learningmanagementsystem.service.UserService;

@RestController
@RequestMapping("/secured/users")
public class UserController {
	
	@Autowired
	private UserService userService;
	
	@GetMapping
	@ResponseStatus(value = HttpStatus.OK)
	public List<User> getAll() {
		return this.userService.findAll();
	}
	
	@GetMapping("/{id}")
	@ResponseStatus(value = HttpStatus.OK)
	public User get(@PathVariable Long id) {
		return this.userService.getById(id);
	}
	
	@PostMapping
	@ResponseStatus(value = HttpStatus.OK)
	public User create(User user) {
		return this.userService.save(user);
	}
	
	
}
